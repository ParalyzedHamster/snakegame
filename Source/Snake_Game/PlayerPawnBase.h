// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;

UCLASS()
class SNAKE_GAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite, Category = "Edge of the map")
	float MaxY = 1100.f;
	UPROPERTY(BlueprintReadWrite, Category = "Edge of the map")
	float MinY=-1100.f;
	UPROPERTY(BlueprintReadWrite, Category = "Edge of the map")
	float MaxX= 1100.f;
	UPROPERTY(BlueprintReadWrite, Category = "Edge of the map")
	float MinX= -1100.f;
	UPROPERTY(BlueprintReadWrite, Category = "Edge of the map")
	float SpawnZ = 40.f;

	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	   void CreateSnakeActor();
	UFUNCTION()
		void CreateFoodActor();
	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);
	UFUNCTION()
		void AddRandomApple();
};
