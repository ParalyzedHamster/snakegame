// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake_Game/SnakeDeathActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSnakeDeathActor() {}
// Cross Module References
	SNAKE_GAME_API UClass* Z_Construct_UClass_ASnakeDeathActor_NoRegister();
	SNAKE_GAME_API UClass* Z_Construct_UClass_ASnakeDeathActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake_Game();
// End Cross Module References
	void ASnakeDeathActor::StaticRegisterNativesASnakeDeathActor()
	{
	}
	UClass* Z_Construct_UClass_ASnakeDeathActor_NoRegister()
	{
		return ASnakeDeathActor::StaticClass();
	}
	struct Z_Construct_UClass_ASnakeDeathActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASnakeDeathActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake_Game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASnakeDeathActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SnakeDeathActor.h" },
		{ "ModuleRelativePath", "SnakeDeathActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASnakeDeathActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASnakeDeathActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASnakeDeathActor_Statics::ClassParams = {
		&ASnakeDeathActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASnakeDeathActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASnakeDeathActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASnakeDeathActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASnakeDeathActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASnakeDeathActor, 4076044989);
	template<> SNAKE_GAME_API UClass* StaticClass<ASnakeDeathActor>()
	{
		return ASnakeDeathActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASnakeDeathActor(Z_Construct_UClass_ASnakeDeathActor, &ASnakeDeathActor::StaticClass, TEXT("/Script/Snake_Game"), TEXT("ASnakeDeathActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASnakeDeathActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
