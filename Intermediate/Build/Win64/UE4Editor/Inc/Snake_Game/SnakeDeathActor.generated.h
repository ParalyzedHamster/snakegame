// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_GAME_SnakeDeathActor_generated_h
#error "SnakeDeathActor.generated.h already included, missing '#pragma once' in SnakeDeathActor.h"
#endif
#define SNAKE_GAME_SnakeDeathActor_generated_h

#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_SPARSE_DATA
#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_RPC_WRAPPERS
#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeDeathActor(); \
	friend struct Z_Construct_UClass_ASnakeDeathActor_Statics; \
public: \
	DECLARE_CLASS(ASnakeDeathActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_Game"), NO_API) \
	DECLARE_SERIALIZER(ASnakeDeathActor)


#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASnakeDeathActor(); \
	friend struct Z_Construct_UClass_ASnakeDeathActor_Statics; \
public: \
	DECLARE_CLASS(ASnakeDeathActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake_Game"), NO_API) \
	DECLARE_SERIALIZER(ASnakeDeathActor)


#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeDeathActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeDeathActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeDeathActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeDeathActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeDeathActor(ASnakeDeathActor&&); \
	NO_API ASnakeDeathActor(const ASnakeDeathActor&); \
public:


#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeDeathActor(ASnakeDeathActor&&); \
	NO_API ASnakeDeathActor(const ASnakeDeathActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeDeathActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeDeathActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeDeathActor)


#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_PRIVATE_PROPERTY_OFFSET
#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_9_PROLOG
#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_SPARSE_DATA \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_RPC_WRAPPERS \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_INCLASS \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_SPARSE_DATA \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_INCLASS_NO_PURE_DECLS \
	Snake_Game_Source_Snake_Game_SnakeDeathActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_GAME_API UClass* StaticClass<class ASnakeDeathActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Game_Source_Snake_Game_SnakeDeathActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
